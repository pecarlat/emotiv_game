package casque;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import org.jbox2d.callbacks.ContactImpulse;
import org.jbox2d.callbacks.ContactListener;
import org.jbox2d.collision.Manifold;
import org.jbox2d.common.Vec2;
import org.jbox2d.dynamics.Body;
import org.jbox2d.dynamics.BodyType;
import org.jbox2d.dynamics.contacts.Contact;

import fr.atis_lab.physicalworld.AnimatedSprite;
import fr.atis_lab.physicalworld.DrawingPanel;
import fr.atis_lab.physicalworld.InvalidActionNameException;
import fr.atis_lab.physicalworld.InvalidSpriteNameException;
import fr.atis_lab.physicalworld.LockedWorldException;
import fr.atis_lab.physicalworld.ObjectNameNotFoundException;
import fr.atis_lab.physicalworld.PhysicalWorld;
import fr.atis_lab.physicalworld.Sprite;


public class MainGui implements ContactListener, ActionListener {

	/********* Variable de la frame *********/
	private static JFrame frame;
    private static JPanel lowPanel;
    private static Thread t;
	public static JButton trainBtt;
	public static JComboBox<String> comboBox;
	
    /******* Variable du jeu *******/
	private static PhysicalWorld world;
	private static Body man, door;
	private static DrawingPanel panel;
	private static int level = 0;
	private static boolean turn = true, gameFinished = false, canLeft = true, canRight = false;
	private static boolean needToTeleportManDoor = false, open = true;
	private static int lvlMax = 3;
	
	
    /***************** Les boutons ****************/
    public void actionPerformed(ActionEvent e) {
    	
		switch(e.getActionCommand()) {
		
			case "key1":

				Unthread.train(comboBox.getSelectedIndex()); // on appel la m�thode train pour enregistrer l'action � penser s�lectionn�e
				
				break;
		
		}
	
	}
    

    /*********** Main ************/
	public static void main(String args[])
	{
		new MainGui(); // on introduit l'interface graphique
		
		t = new Thread(new Unthread()); // on lance le processus qui va "capturer" nos pens�es en permanance
		t.start();
		
		jeu(); // on lance le jeu
		
    }
	
	/*********** Interface  ***********/
	MainGui()
    {
		
		frame = new JFrame("Casque Neuronal");
		   
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setMinimumSize(new Dimension(930,600));
		frame.setLayout(new BorderLayout());
		
		frame.addWindowListener(new WindowAdapter(){
            public void windowClosing(WindowEvent e){ // lorsque l'on ferme la fen�tre,
            	turn = false;						  // on arr�te le jeu
				Unthread.finish();					  // on d�connecte le casque
				t.interrupt();						  // et on arr�te le processus
            }
		});
		
		

	    JPanel centerPanel = new JPanel();
	    
	    JPanel topPanel = new JPanel(new GridLayout(1,2));
		
        // Cr�ation de la liste des actions
	    String [] options = {"Neutral","Left", "Right", "Disappear"};
	    comboBox = new JComboBox<String>(options);
	    topPanel.add(comboBox);
	    
	    // Cr�ation du bouton "Train"
		trainBtt = new JButton("Train");
		topPanel.add(trainBtt);
		
		trainBtt.setActionCommand("key1");
		trainBtt.addActionListener(this);

		// Cr�ation du monde (jeu)
		world = new PhysicalWorld(new Vec2(0,-50f), -110, 110, -55, 55, Color.RED);
		world.setContactListener(this);
		
		panel = new DrawingPanel(world, new Dimension(930,480), 4f);
		panel.setBackGroundIcon(new ImageIcon("./img/plage.jpg"));
		
		centerPanel.add(panel);

		lowPanel =  new JPanel(new GridLayout(3,1));
		lowPanel.add(new JLabel("Current level : " + level, JLabel.CENTER));
	    lowPanel.add(new JLabel("Try to do disappear the cube", JLabel.CENTER));
	   
	    
	    frame.add(topPanel, BorderLayout.NORTH);
		frame.add(lowPanel, BorderLayout.SOUTH);	
		frame.add(centerPanel, BorderLayout.CENTER);

		
		frame.pack();
		frame.setVisible(true);
		frame.setLocationRelativeTo(null);
		
	}
	
	
	/************* LE JEU ******************/
	
	/************* Lancement du jeu *********/
	public static void jeu() {
		
		boolean disappear = true;
		int compt = 1;
		double power = 0.6;
		
		try {
        	
	     	   float timeStep = 1/60.0f;
	     	   int msSleep = Math.round(1000*timeStep);
	        	   
	     	   world.setTimeStep(timeStep);
			
	     	   chooseLevel(); // charge le premier niveau
	     	
	     	   while(turn) {
	     		   
						world.step();

						switch(level) {
						
							case 0:
								if(Unthread.getDisappear()) {
										
									if(Unthread.getAction() == 8192  && Unthread.getPower() >= power && disappear == true) {
										
										try {
											world.destroyObject("carre1");
										}catch(LockedWorldException ex) {
											ex.printStackTrace();
											System.exit(-1);
										}
										catch (ObjectNameNotFoundException ex) {
											ex.printStackTrace();
											System.exit(-1);
										}
											
										disappear = false;
											
										needToTeleportManDoor = true;
									}
									
									if(needToTeleportManDoor) {
										
										man.setTransform(new Vec2(0,-51), 0);
										man.setLinearVelocity(new Vec2(0,0));
													
										needToTeleportManDoor = false;
										disappear = true;
										
										panel.updateUI();
										Thread.sleep(2000); // sinon pas le temps de voir le cube disparaitre
										
										level ++;
										chooseLevel();
										
										try {
											AnimatedSprite.extractAnimatedSprite(door).setCurrentAction("open");
										}
										catch(InvalidActionNameException ex) {
											ex.printStackTrace();
											System.exit(-1);
										}

							        	lowPanel.removeAll();
										lowPanel.add(new JLabel("Current level : " + level, JLabel.CENTER));
										lowPanel.add(new JLabel("Try to think right to move", JLabel.CENTER));
											
									}
									
								}
							break;
							
							case 1:
								
								if(Unthread.getRight()) {
									
									if(Unthread.getAction() == 8192  && Unthread.getPower() >= power && disappear == true) {
										
										try {
											world.destroyObject("carre1");
										}catch(LockedWorldException ex) {
											ex.printStackTrace();
											System.exit(-1);
										}catch (ObjectNameNotFoundException ex) {
											ex.printStackTrace();
											System.exit(-1);
										}
										
										disappear = false;
									
									}
									
									if(Unthread.getAction() == 64  && Unthread.getPower() >= power && canRight == false) {
										canRight = true;
									}
									
									if(canRight)
										man.setLinearVelocity(new Vec2(30,0));
									

								}
								
								if(needToTeleportManDoor) {
									
									man.setTransform(new Vec2(0,-51), 0);
									man.setLinearVelocity(new Vec2(0,0));
												
									needToTeleportManDoor = false;
									open = false;
									disappear = true;
									canRight = false;
									
									try {
										AnimatedSprite.extractAnimatedSprite(door).setCurrentAction("close");
									}
									catch(InvalidActionNameException ex) {
										ex.printStackTrace();
										System.exit(-1);
									}
									
									chooseLevel();
										
								}
								break;
							
							case 2:
								if(needToTeleportManDoor) {
									
									man.setTransform(new Vec2(0,-51), 0);
									man.setLinearVelocity(new Vec2(0,0));
												
									needToTeleportManDoor = false;
									open = false;
									disappear = true;
									canRight = false;
									canLeft = false;
									
									try {
										AnimatedSprite.extractAnimatedSprite(door).setCurrentAction("close");
									}
									catch(InvalidActionNameException ex) {
										ex.printStackTrace();
										System.exit(-1);
									}
									
									chooseLevel();
										
								}
								
								if(Unthread.getLeft()) {
									
									if(Unthread.getAction() == 8192  && Unthread.getPower() >= power && disappear == true) {
										
										try {
											world.destroyObject("carre1");
										}catch(LockedWorldException ex) {
											ex.printStackTrace();
											System.exit(-1);
										}catch (ObjectNameNotFoundException ex) {
											ex.printStackTrace();
											System.exit(-1);
										}
										
										disappear = false;
									
									}
									
									if(Unthread.getAction() == 64  && Unthread.getPower() >= power && canRight == false) {
										
										canRight = true;
										canLeft = false;
										
										try {
										AnimatedSprite.extractAnimatedSprite(man).setCurrentAction("right1");
										}
										catch(InvalidActionNameException ex) {
											ex.printStackTrace();
											System.exit(-1);
										}
										
									}
									
									if(canRight)
										man.setLinearVelocity(new Vec2(30,0));
									
									if(Unthread.getAction() == 32  && Unthread.getPower() >= power && canLeft == false) {
									
										canLeft = true;
										canRight = false;
										
										try {
											AnimatedSprite.extractAnimatedSprite(man).setCurrentAction("left1");
										}
										catch(InvalidActionNameException ex) {
											ex.printStackTrace();
											System.exit(-1);
										}
										
									}
									
									if(canLeft)
										man.setLinearVelocity(new Vec2(-30,0));
									
								}
								break;
								
							case 3:
								if(needToTeleportManDoor) {
									
									man.setTransform(new Vec2(0,-51), 0);
									man.setLinearVelocity(new Vec2(0,0));
												
									needToTeleportManDoor = false;
									open = false;
									disappear = true;
									canRight = false;
									canLeft = false;
									
									try {
										AnimatedSprite.extractAnimatedSprite(door).setCurrentAction("close");
									}
									catch(InvalidActionNameException ex) {
										ex.printStackTrace();
										System.exit(-1);
									}
									
									chooseLevel();
										
								}
								
								if(Unthread.getAction() == 8192  && Unthread.getPower() >= power && disappear == true) {
									
									try {
										
										if(compt == 1) {
											world.destroyObject("carre2");
											panel.updateUI();
											Thread.sleep(1000);
										}
										
										if(compt == 2)
											world.destroyObject("carre1");
										
									}catch(LockedWorldException ex) {
										ex.printStackTrace();
										System.exit(-1);
									}catch (ObjectNameNotFoundException ex) {
										ex.printStackTrace();
										System.exit(-1);
									}
									
									if(compt == 2)
										disappear = false;
									
									compt ++;
								
								}
								
								if(Unthread.getAction() == 64  && Unthread.getPower() >= power && canRight == false) {
									
									canRight = true;
									canLeft = false;
									
									try {
									AnimatedSprite.extractAnimatedSprite(man).setCurrentAction("right1");
									}
									catch(InvalidActionNameException ex) {
										ex.printStackTrace();
										System.exit(-1);
									}
									
								}
								
								if(canRight)
									man.setLinearVelocity(new Vec2(30,0));
								
								if(Unthread.getAction() == 32  && Unthread.getPower() >= power && canLeft == false) {
								
									canLeft = true;
									canRight = false;
									
									try {
										AnimatedSprite.extractAnimatedSprite(man).setCurrentAction("left1");
									}
									catch(InvalidActionNameException ex) {
										ex.printStackTrace();
										System.exit(-1);
									}
									
								}
								
								if(canLeft)
									man.setLinearVelocity(new Vec2(-30,0));
								break;
						
						}
						
						
						if(gameFinished) {
							
					  		world.addRectangularObject(140f, 110f, BodyType.STATIC, new Vec2(0, 0), 0, new Sprite("homer", 4, null, new ImageIcon("./img/homer.jpg")));
					
					  		lowPanel.removeAll();
						    lowPanel.add(new JLabel("Congragulation ! You finish the game thanks to your mind !", JLabel.CENTER));
						    
							turn = false;
						
						}
						
						
						Thread.sleep(msSleep);
						panel.updateUI();
						
				}
		            
				}
				catch(InterruptedException ex) {
					System.err.println(ex.getMessage());
		        	}
				catch (InvalidSpriteNameException ex) {
					System.err.println(ex.getMessage());
					System.exit(-1);
				}
		
	}
	
	/******* Chargement des niveaux **********/
	public static void chooseLevel() {
		   
		switch(level) {
	   	
	   		case 0:
	   			level0();
	   			break;
	   			
			case 1 :
				level1();
				break;
				
			case 2 :
				level2();
				break;
				
			case 3 :
				level3();
				break;
		
		}
	
	}

	/********** ACTIONS POUR LES CONTACTS ENTRE OBJETS DU JEU ************/
	
	public void beginContact(Contact contact) {
	
       String nameA = Sprite.extractSprite(contact.getFixtureA().getBody()).getName();
	   String nameB = Sprite.extractSprite(contact.getFixtureB().getBody()).getName();
        	
		if((nameA.startsWith("carre") && nameB.startsWith("man")) || (nameA.startsWith("man") && nameB.startsWith("carre"))) {
			
			canRight = false;
			canLeft = false;
			
		}
		
		if((nameA.startsWith("bouton") && nameB.startsWith("man")) || (nameA.startsWith("man") && nameB.startsWith("bouton"))) {
			// si on touche le bouton on ouvert la porte
			
			open = true;
			canLeft = false;
			
			try {
				AnimatedSprite.extractAnimatedSprite(door).setCurrentAction("open");
			}
			catch(InvalidActionNameException ex) {
				ex.printStackTrace();
				System.exit(-1);
			}
			
		}
			
        if(contact.getFixtureB().equals(door.getFixtureList()) && level == lvlMax && open)
        	gameFinished = true; // arr�te le jeu si on est au dernier niveau et qu'on touche la porte ouverte
        	
        if(contact.getFixtureB().equals(door.getFixtureList()) && level < lvlMax && open) {
        	// si on touche la porte ouverte et qu'on est pas au dernier niveau
        	
        	level++; 
        	needToTeleportManDoor = true;
        	
        	lowPanel.removeAll();
			lowPanel.add(new JLabel("Current level : " + level, JLabel.CENTER));
			
			if(level == 2)
				 lowPanel.add(new JLabel("Try to think left to move and active the button to open the door", JLabel.CENTER));
       		
        }
        	      	      	
	}

	public void endContact(Contact contact) {}
	
	public void postSolve(Contact contact, ContactImpulse impulse) {}
	
	public void preSolve(Contact contact, Manifold oldManifold) {}
		
	
	/********** INSTALLATION DES DIFFERENTS NIVEAUX ************/
	
	public static void level0() {
	
		try {
		
		   world.addRectangularObject(220f, 0.1f, BodyType.STATIC, new Vec2(0, -55), 0, new Sprite("floor", 1, null, null));
	 
		   AnimatedSprite manSprite = new AnimatedSprite("man", 1, null);
		   
		   manSprite.addNewAction("left1", new ImageIcon("./img/manInv1.png"), -1, null);
		   manSprite.setCurrentAction("left1");         
		   
		   manSprite.addNewAction("right1", new ImageIcon("./img/man1.png"), -1, null);
		   manSprite.setCurrentAction("right1");
		   
	   	   man = world.addRectangularObject(4f, 8f, BodyType.DYNAMIC, new Vec2(0, -51), 0, manSprite);
		   man.setFixedRotation(true);
		   
		   AnimatedSprite doorSprite = new AnimatedSprite("door", 1, null);
		   
		   doorSprite.addNewAction("open", new ImageIcon("./img/door.png"), -1, null);
		   doorSprite.setCurrentAction("open");         
		   
		   doorSprite.addNewAction("close", new ImageIcon("./img/door close.png"), -1, null);
		   doorSprite.setCurrentAction("close");
		   
		   door = world.addRectangularObject(10f, 13f, BodyType.STATIC, new Vec2(101, -50), 0, doorSprite);
		   
		   world.addRectangularObject(7f, 7f, BodyType.STATIC, new Vec2(80, -52), 0, new Sprite("carre1", 1, Color.RED,  new ImageIcon("./img/cube.png")));
		
   	       }
		catch (InvalidSpriteNameException ex) {
			ex.printStackTrace();
			System.exit(-1);
		}
		catch (InvalidActionNameException ex) {
        	ex.printStackTrace();
        	System.exit(-1);
		}
	
	}
		
	public static void level1() {
		
		try {
			world.addRectangularObject(7f, 7f, BodyType.STATIC, new Vec2(80, -52), 0, new Sprite("carre1", 1, Color.RED,  new ImageIcon("./img/cube.png")));
		}
		catch (InvalidSpriteNameException ex) {
			ex.printStackTrace();
			System.exit(-1);
		}
	
	}
	
	public static void level2() {
		
		try {
			
			world.addRectangularObject(7f, 7f, BodyType.STATIC, new Vec2(80, -52), 0, new Sprite("carre1", 1, Color.RED, new ImageIcon("./img/cube.png")));
			
			world.addRectangularObject(11f, 11f, BodyType.STATIC, new Vec2(-100, -50), 0, new Sprite("bouton", 1, null, new ImageIcon("./img/boutton.png")));
		
		} catch (InvalidSpriteNameException ex) {
			ex.printStackTrace();
			System.exit(-1);
		}
		
		
	}
	
	public static void level3() {
		
		try {
			
			world.addRectangularObject(7f, 7f, BodyType.STATIC, new Vec2(80, -52), 0, new Sprite("carre1", 1, Color.RED, new ImageIcon("./img/cube.png")));
			world.addRectangularObject(7f, 7f, BodyType.STATIC, new Vec2(-80, -52), 0, new Sprite("carre2", 2, Color.RED, new ImageIcon("./img/cube.png")));
		
		} catch (InvalidSpriteNameException ex) {
			ex.printStackTrace();
			System.exit(-1);
		}
		
	}
	
}