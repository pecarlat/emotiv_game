package casque;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

import com.sun.jna.Pointer;
import com.sun.jna.ptr.IntByReference;

public class Unthread extends Thread {

	private static int index = 0;
	private static double power = 0;
	private static int action = 0;
	private static boolean turn = true, right = false, disappear = false, left = false;
	public static int[] cognitivActionList = // Liste des actions possibles
		{  EmoState.EE_CognitivAction_t.COG_NEUTRAL.ToInt(),
		   EmoState.EE_CognitivAction_t.COG_LEFT.ToInt(),
		   EmoState.EE_CognitivAction_t.COG_RIGHT.ToInt(),
		   EmoState.EE_CognitivAction_t.COG_DISAPPEAR.ToInt(),
		};
   public static Boolean[] cognitivActionsEnabled = new Boolean[cognitivActionList.length];
	
	
	public Unthread() {
		
		cognitivActionsEnabled[0] = true;
		
        for (int i = 1; i < cognitivActionList.length; i++)
        {
            cognitivActionsEnabled[i] = false;
        }
		
	}
	
	/********* Emotiv **********/
	
    public static void StartTrainingCognitiv(EmoState.EE_CognitivAction_t cognitivAction)
    {
        if (cognitivAction == EmoState.EE_CognitivAction_t.COG_NEUTRAL) // N'arrive jamais
        {
        	Edk.INSTANCE.EE_CognitivSetTrainingAction(0,EmoState.EE_CognitivAction_t.COG_NEUTRAL.ToInt());
			Edk.INSTANCE.EE_CognitivSetTrainingControl(0, Edk.EE_CognitivTrainingControl_t.COG_START.getType());
        }
        else
            for (int i = 1 ; i < cognitivActionList.length ; i++)
                if (cognitivAction.ToInt() == cognitivActionList[i]) // On checke si on est dans la bonne action, et si elle est autoris�e
                    if (cognitivActionsEnabled[i]) // Pour l'utilisateur 0, on d�finit un start sur l'action
                    {
                    	Edk.INSTANCE.EE_CognitivSetTrainingAction(0, cognitivAction.ToInt());
                    	Edk.INSTANCE.EE_CognitivSetTrainingControl(0, Edk.EE_CognitivTrainingControl_t.COG_START.getType());
                    }
    }
    
    public static void EnableCognitivAction(EmoState.EE_CognitivAction_t cognitivAction, Boolean iBool)
    {
        for (int i = 1 ; i < cognitivActionList.length ; i++)
            if (cognitivAction.ToInt() == cognitivActionList[i])
                cognitivActionsEnabled[i] = iBool;
    }
    
    public static void EnableCognitivActionsList()
    {
        long cognitivActions = 0x0000;
        
        for (int i = 1 ; i < cognitivActionList.length ; i++)
            if (cognitivActionsEnabled[i])
                cognitivActions = cognitivActions | ((long)cognitivActionList[i]);
        
        Edk.INSTANCE.EE_CognitivSetActiveActions(0, cognitivActions);
    }
	

	
	public static void train(int indexe) {
		
		index = indexe;
		
		if(index == 0)
		{
			Edk.INSTANCE.EE_CognitivSetTrainingAction(0,EmoState.EE_CognitivAction_t.COG_NEUTRAL.ToInt());
			Edk.INSTANCE.EE_CognitivSetTrainingControl(0, Edk.EE_CognitivTrainingControl_t.COG_START.getType());
		}
		
		if(index == 1) // Sauter s�lectionn�
		{
			try
			{
				EnableCognitivAction(EmoState.EE_CognitivAction_t.COG_LEFT, true); // On met "true" dans la case du tableau correspondante
				EnableCognitivActionsList(); // D�finit s'il existe au moins l'une des actions en true, les actions courrantes actives
				StartTrainingCognitiv(EmoState.EE_CognitivAction_t.COG_LEFT);
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}
		
		if(index == 2) // Droite s�lectionn�
		{
			try
			{
				EnableCognitivAction(EmoState.EE_CognitivAction_t.COG_RIGHT, true); // On met "true" dans la case du tableau correspondante
				EnableCognitivActionsList(); // D�finit s'il existe au moins l'une des actions en true, les actions courrantes actives
				StartTrainingCognitiv(EmoState.EE_CognitivAction_t.COG_RIGHT);
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}
		
		if(index == 3) // Disparaitre s�lectionn�
		{
			try
			{
				EnableCognitivAction(EmoState.EE_CognitivAction_t.COG_DISAPPEAR, true); // On met "true" dans la case du tableau correspondante
				EnableCognitivActionsList(); // D�finit s'il existe au moins l'une des actions en true, les actions courrantes actives
				StartTrainingCognitiv(EmoState.EE_CognitivAction_t.COG_DISAPPEAR);
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}
		
	}

	public void run() {

		Pointer eEvent = Edk.INSTANCE.EE_EmoEngineEventCreate();

		Pointer eState = Edk.INSTANCE.EE_EmoStateCreate();
		IntByReference userID = null;
		short composerPort = 1726;
		int state = 0;
		int option = 2;

		userID = new IntByReference(0);

		switch (option)
    	{
    		case 1: // On travaille avec le casque
    		{
    			if (Edk.INSTANCE.EE_EngineConnect("Emotiv Systems-5") != EdkErrorCode.EDK_OK.ToInt()) // Le casque ne se connecte pas, d�faut hardware
    			{
    				System.out.println("Emotiv Engine start up failed."); // Pour autant, pas de print avec option = 1 et pas de casque
    				return;
    			}
    			else 
    				System.out.println("Connected on [127.0.0.1]");
    			break;
    		}
    		case 2: // On travaille avec EmoComposer
    		{	
    			if (Edk.INSTANCE.EE_EngineRemoteConnect("127.0.0.1", composerPort, "Emotiv Systems-5") != EdkErrorCode.EDK_OK.ToInt()) // N'arrive pas � se connecter � emocomposer
    			{
    				System.out.println("Cannot connect to EmoComposer on [127.0.0.1]");
    				//JOptionPane jop = new JOptionPane();
    				return;
    			}
    			else 
    				System.out.println("Connected on [127.0.0.1]");
    			break;
    		}
    		default: // Autre cas
    		{
    			System.out.println("Invalid option...");
    			return;
    		}

    	}
		
		

		while (turn) {

			state = Edk.INSTANCE.EE_EngineGetNextEvent(eEvent);

			// New event needs to be handled
			if (state == EdkErrorCode.EDK_OK.ToInt()) {
				int eventType = Edk.INSTANCE.EE_EmoEngineEventGetType(eEvent);
				Edk.INSTANCE.EE_EmoEngineEventGetUserId(eEvent, userID);
				if (eventType == Edk.EE_Event_t.EE_UserAdded.ToInt()) {
					EmoProfileManagement.AddNewProfile("3");
					JOptionPane.showMessageDialog(new JFrame(), "User add",
							"Dialog", JOptionPane.OK_OPTION);
				}

				if (eventType == Edk.EE_Event_t.EE_CognitivEvent.ToInt()) {
					int cogType = Edk.INSTANCE.EE_CognitivEventGetType(eEvent);

					if (cogType == Edk.EE_CognitivEvent_t.EE_CognitivTrainingStarted
							.getType())
						JOptionPane.showMessageDialog(new JFrame(),
								"Cognitiv Training Start", "Dialog",
								JOptionPane.OK_OPTION);

					if (cogType == Edk.EE_CognitivEvent_t.EE_CognitivTrainingCompleted
							.getType())
						JOptionPane.showMessageDialog(new JFrame(),
								"Cognitiv Training Complete", "Dialog",
								JOptionPane.OK_OPTION);

					if (cogType == Edk.EE_CognitivEvent_t.EE_CognitivTrainingSucceeded
							.getType()) {
						Edk.INSTANCE.EE_CognitivSetTrainingControl(0,
								Edk.EE_CognitivTrainingControl_t.COG_ACCEPT
										.getType());
						JOptionPane.showMessageDialog(new JFrame(),
								"Cognitiv Training Succeeded", "Dialog",
								JOptionPane.OK_OPTION);
						
						if(index == 2)
							right = true;
						
						if(index == 3)
							disappear = true;
						
						if(index == 1)
							left = true;
						
					}
					
					if (cogType == Edk.EE_CognitivEvent_t.EE_CognitivTrainingFailed
							.getType())
						JOptionPane.showMessageDialog(new JFrame(),
								"Cognitiv Training Failed", "Dialog",
								JOptionPane.ERROR_MESSAGE);

					if (cogType == Edk.EE_CognitivEvent_t.EE_CognitivTrainingRejected
							.getType())
						JOptionPane.showMessageDialog(new JFrame(),
								"Cognitiv Training Rejected", "Dialog",
								JOptionPane.ERROR_MESSAGE);
				}

				if (eventType == Edk.EE_Event_t.EE_EmoStateUpdated.ToInt()) {
					Edk.INSTANCE.EE_EmoEngineEventGetEmoState(eEvent, eState);

					action = EmoState.INSTANCE
							.ES_CognitivGetCurrentAction(eState);
					power = EmoState.INSTANCE
							.ES_CognitivGetCurrentActionPower(eState);
					if (power != 0) {
						
						if(action == 8192)
							System.out.println("Action : Disappear");
						
						if(action == 64)
							System.out.println("Action : Right");
						
						if(action == 32)
							System.out.println("Action : Left");
						
						System.out.println("Power:" + power);
					}
				}
				
			} else if (state != EdkErrorCode.EDK_NO_EVENT.ToInt()) {
				System.out.println("Internal error in Emotiv Engine!");
			}

		}

	}

	public static double getPower() {
		return power;
	}
	
	public static double getAction() {
		return action;
	}
	
	public static boolean getRight() {
		return right;
	}
	
	public static boolean getDisappear() {
		return disappear;
	}
	
	public static boolean getLeft() {
		return left;
	}
	
	public static void finish() {
		
		turn = false;
		
		Edk.INSTANCE.EE_EngineDisconnect();
    	System.out.println("Disconnected!");
		
	}

}