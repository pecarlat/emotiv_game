# Simple BCI game
Simple game, controllable by Epoc Emotiv headset (EEG) with couple commands
(left, right, disappear, ...).

## Project architecture
- Interface 0: Basic code, as provided by Emotiv, for testing
- Interface 1: Code of our game, can be used with EmoComposer or the headset
(must use the modifier option). The game here is very simple, three levels, and
we can select the trainings by ourselves (lower section of the window)
- Interface 2: The game here is more intuitive, more guided, but works only
with EmoComposer (for the headset, probably need to add the 'neutral' training)


## The game itself
- Level 1: Character goes straight to the right, and must thinks 'disappear'
when he meets an obstacle
- Level 2: Same, but we need to think 'right' to move
- Level 3: Must think left, right and disappear to avoid obstacles and complete
the game

It looks like this:
![Game visulaization](game_visu.png)



## Setup
We need Eclipse 32bits and JDE7, and to import the libraries provided in the
repo.
